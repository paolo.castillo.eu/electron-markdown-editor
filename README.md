# How to Build a markdown editor using electron, ReactJS, Vite, CodeMirror, and Remark

1. Clone git repo vite-electron-builder
```
$ git clone https://github.com/cawa-93/vite-electron-builder
```

2. Unistall and Install component with npm (switch framework from vue to react)

- Uninstall vue.
```
$ npm uninstall @vitejs/plugin-vue @vue/compiler-sfc eslint-plugin-vue vue vue-router

```
- Install react.
```
$ npm i react react-dom
```

- Install react and configure eslint.
```
$ npm i -D @types/react @types/react-dom @typescript-eslint/parser
```

- Uninstall vue tsc.
```
$ npm uninstall vue-tsc
```

- Install and configure eslint, prettier, and typescript.
```
$ npm i -D eslint eslint-config-prettier @typescript-eslint/eslint-plugin @typescript-eslint/parser
```

- Remove eslintrc.json
```
$ rm packages/renderer/.eslintrc.json
```

- Edit file .eslintrc.json

```
{
  "root": true,
  "env": {
    "es2021": true,
    "node": true,
    "browser": false
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier"
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 12,
    "sourceType": "module"
  },
  "plugins": [
    "@typescript-eslint"
  ],
  "ignorePatterns": [
    "types/env.d.ts",
    "node_modules/**",
    "**/dist/**"
  ],
  "rules": {
    "@typescript-eslint/no-unused-vars": "error",
    "@typescript-eslint/no-var-requires": "off",
    "@typescript-eslint/consistent-type-imports": "error"
  }
}
```

- Edit file tsconfig.json ``/packages/renderer/tsconfig.json`` 
```
{
  "extends": "../../tsconfig.json",
  "compilerOptions": {
    "baseUrl": ".",
    "jsx": "react",
    "allowSyntheticDefaultImports": true,
    "paths": {
      "/@/*": [
        "./src/*"
      ]
    },
    "lib": ["ESNext", "dom", "dom.iterable"]
  },

  "include": [
    "src/**/*.ts",
    "src/**/*.tsx",
    "types/**/*.d.ts",
    "../../types/**/*.d.ts",
    "../preload/types/electron-api.d.ts"
  ]
}

```

- Create prettier config in index directory ``prettier.config.js``

```
const options = {
    arrowParens: 'avoid',
    singleQuote: true,
    backetSpacing: true,
    endOfLine: 'lf',
    semi: false,
    tabWidth: 2,
    trailingComma: 'none'
}

module.exports = options
```

3. Install CodeMirror6

```
$ npm i @codemirror/state @codemirror/commands @codemirror/history @codemirror/language @codemirror/matchbrackets @codemirror/gutter @codemirror/highlight @codemirror/lang-javascript
```